﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notion.Unpacker
{
    class Program
    {
        static void Main(string[] args)
        {
            string workingDirectory = @"..\..\..\Notion.Server\wwwroot";
            Environment.CurrentDirectory = workingDirectory;

            string inputFile = "app-c1ce7ab9724542bdb44e.js";
            string outputFile = @"app\app.js";

            using (StreamReader reader = new StreamReader(inputFile))
            {
                StreamWriter writer = new StreamWriter(outputFile);

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    if (line.StartsWith("/*!******"))
                        continue;

                    if (line.StartsWith("  !*** ./"))
                    {
                        outputFile = Path.Combine("app", line.Trim(new[] { ' ', '!', '*' }));

                        string outputDirectory = Path.GetDirectoryName(outputFile);
                        if (!Directory.Exists(outputDirectory))
                            Directory.CreateDirectory(outputDirectory);

                        writer.Dispose();
                        writer = new StreamWriter(outputFile);

                        Console.WriteLine(outputFile.Replace("\\", "/"));

                        reader.ReadLine();
                        continue;
                    }

                    if (line.StartsWith("/*"))
                    {
                        int separator = line.IndexOf("*/");

                        string comment = line.Substring(0, separator + 2);
                        writer.WriteLine(comment);

                        line = line.Substring(separator + 2);
                        if (line.Trim() == "")
                            continue;
                    }

                    if (line.StartsWith("function(e,t,r){\"use strict"))
                    {
                        writer.WriteLine("var e = {};");
                        writer.WriteLine("var t = { cursorDefault: {}, cursorPointer: {}, fadeIn: {}, mobile: {}, fadeInFast: {}, fadeInSlow: {}, cordova: {}, mobileBrowser: {}, mobileTapping: {}, frontPage: {}, default: {}, adminUser: {}, userPersonas: {}, userPersonaDisplayNames: {}, spaceAllowedPropertyChanges: {} };");
                        writer.WriteLine("var r = function(i) { return {} };");
                        writer.WriteLine();

                        line = line
                            .Substring(16, line.Length - 18)
                            .Replace("===", "~~~")
                            .Replace("==", "~~")
                            .Replace("=", " = ")
                            .Replace("&&", " && ")
                            .Replace("||", " || ")
                            .Replace("return\"", "return \"")
                            .Replace(":", ": ")
                            .Replace(",", ", ")
                            .Replace("~~", " == ")
                            .Replace("~~~", " === ");

                        int index = 0;
                        int indentLevel = 0;

                        while (true)
                        {
                            int separator = line.IndexOfAny(new[] { ';', '{', '}' }, index);

                            if (separator == -1)
                            {
                                writer.WriteLine(line.Substring(index));
                                break;
                            }
                            
                            string indent = new string(' ', indentLevel * 4);
                            writer.Write(indent + line.Substring(index, separator - index));

                            if (line[separator] == '}')
                                indentLevel--;
                            if (indentLevel < 0)
                                indentLevel = 0;

                            indent = new string(' ', indentLevel * 4);

                            if (line[separator] == ';')
                                writer.WriteLine(";");
                            else if (line[separator] == '{')
                                writer.WriteLine(" {");
                            else
                            {
                                writer.WriteLine();
                                writer.WriteLine(indent + line[separator]);
                            }

                            if (line[separator] == '{')
                                indentLevel++;

                            index = separator + 1;
                        }
                    }
                    else
                        writer.WriteLine(line);
                }

                writer.Dispose();
            }
        }
    }
}
