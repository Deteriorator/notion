# Notion private server

This is a private server for Notion. It can be run either on a classic environment (executable) or in Docker.

Follow this link to get the prebuilt Docker images: https://hub.docker.com/r/jbatonnet/notion

## Docker

### Images
- **latest**: Latest image using `microsoft/dotnet` for all architectures
- **rpi**: Latest image built for Raspberry Pi or other arm32 devices

### Volumes
- **/app/Database**: Notion objects and database
- **/app/Files**: Uploaded files
- **/app/Authentication**: Authentication tokens to keep user sessions across restarts

### Environment
- **NOTION_AUTH_ENABLED** = True
- **NOTION_AUTH_CLIENTID** = [GOOGLE_OAUTH_CLIENTID]
- **NOTION_AUTH_CLIENTSECRET** = [GOOGLE_OAUTH_CLIENTSECRET]
- **NOTION_AUTH_AUTOREGISTRATION** = False
- **NOTION_AUTH_ALLOWEDREGISTRATIONS** = xxx@gmail.com
- **NOTION_SERVER_HTTPS** = True
- **NOTION_SERVER_PORT** = 80
- **NOTION_SERVER_FIREBASEURL** = [FIREBASE_URL]

### Example
`
docker run -d -v /data/web/notion/Database:/app/Database -v /data/web/notion/Files:/app/Files -v /data/web/notion/Authentication:/app/Authentication -e NOTION_SERVER_HTTPS=True -e NOTION_AUTH_CLIENTID=[GOOGLE_OAUTH_CLIENTID] -e NOTION_AUTH_CLIENTSECRET=[GOOGLE_OAUTH_CLIENTSECRET] -e NOTION_AUTH_ALLOWEDREGISTRATIONS=xxx@gmail.com,yyy@gmail.com -e NOTION_SERVER_FIREBASEURL=[FIREBASE_URL] -p 5026:80 jbatonnet/notion:rpi
`