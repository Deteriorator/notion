﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Notion.Dumper
{
    class Program
    {
        private const string notionUrl = "https://www.notion.so";

        private const string appPath = @"..\..\..\Notion.Server\wwwroot\app-c3bc03c34a21039f422d.js";
        private const string databaseDirectory = @"..\..\..\Notion.Server\Database";
        private const string templatesDirectory = @"..\..\..\Notion.Server\Services\Templates";

        private static HttpClient httpClient;

        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var cookieContainer = new CookieContainer();

            foreach (string arg in args)
            {
                int separator = arg.IndexOf("=");

                string key = arg.Remove(separator);
                string value = arg.Substring(separator + 1).TrimEnd(';');

                cookieContainer.Add(new Cookie(key, value, "/", "www.notion.so"));
            }

            HttpClientHandler httpClientHandler = new HttpClientHandler() { CookieContainer = cookieContainer };
            httpClient = new HttpClient(httpClientHandler);

            //DumpUserContent(httpClient);
            DumpTemplates(httpClient);
        }

        private static void DumpUserContent(HttpClient client)
        {
            if (Directory.Exists(databaseDirectory))
            {
                string[] subDirectories = Directory.GetDirectories(databaseDirectory);
                foreach (string subDirectory in subDirectories)
                {
                    if (subDirectory.Contains(".git"))
                        continue;

                    Directory.Delete(subDirectory, true);
                }
            }

            dynamic userContent = ApiCall("loadUserContent");
            DumpObjects(userContent.recordMap);

            string[] pageIds = (userContent.recordMap.block as JObject).Properties().Select(p => p.Name).ToArray();

            foreach (string pageId in pageIds)
            {
                dynamic parameters = new JObject();
                parameters.blockId = pageId;
                parameters.shallow = false;

                dynamic blockSubTree = ApiCall("loadBlockSubTree", parameters);
                DumpObjects(blockSubTree.subtreeRecordMap);
            }
        }
        private static void DumpTemplates(HttpClient client)
        {
            if (!Directory.Exists(templatesDirectory))
                Directory.CreateDirectory(templatesDirectory);

            if (Directory.Exists(templatesDirectory))
            {
                string[] subFiles = Directory.GetFiles(templatesDirectory);
                foreach (string subFile in subFiles)
                    File.Delete(subFile);
            }

            string appContent = File.ReadAllText(appPath);

            int templatesStart = appContent.IndexOf("templatePreviewSections=[");
            int templatesEnd = appContent.IndexOf("allTemplateItems", templatesStart);

            string templatesContent = appContent.Substring(templatesStart, templatesEnd - templatesStart);
            MatchCollection templatesMatches = Regex.Matches(templatesContent, "name:\"(?<Name>[^\"]+)\"[^}]+previewRootId:\"(?<Id>[0-9a-f-]+)\"");

            foreach (Match templateMatch in templatesMatches)
            {
                string name = templateMatch.Groups["Name"].Value;
                string id = templateMatch.Groups["Id"].Value;

                string safeName = name
                    .Replace("&", "And")
                    .Replace(" ", "")
                    .Replace("-", "");

                dynamic parameters = new JObject();
                parameters.blockId = id;
                parameters.shallow = false;

                dynamic blockSubTree = ApiCall("loadBlockSubTree", parameters);
                string json = blockSubTree.ToString();

                File.WriteAllText(Path.Combine(templatesDirectory, safeName + ".json"), json);
                Console.WriteLine($"Downloaded {safeName}.json ({id})");
            }

            {
                dynamic parameters = new JObject();
                parameters.blockId = "e950a58c-0d6f-42c3-8225-9e7adc5d4182"; // FIXME: Extract it from app t.desktopGettingStartedPage
                parameters.shallow = false;

                dynamic blockSubTree = ApiCall("loadBlockSubTree", parameters);
                string json = blockSubTree.ToString();

                File.WriteAllText(Path.Combine(templatesDirectory, "GettingStarted.json"), json);
                Console.WriteLine($"Downloaded GettingStarted.json");
            }

            {
                dynamic parameters = new JObject();
                parameters.blockId = "27fa9add-6ec7-433a-88c9-58279f5055c3"; // FIXME: Extract it from app t.mobileGettingStartedPage
                parameters.shallow = false;

                dynamic blockSubTree = ApiCall("loadBlockSubTree", parameters);
                string json = blockSubTree.ToString();

                File.WriteAllText(Path.Combine(templatesDirectory, "GettingStartedMobile.json"), json);
                Console.WriteLine($"Downloaded GettingStartedMobile.json");
            }
        }

        private static dynamic ApiCall(string method, dynamic parameters = null)
        {
            var content = new StringContent(parameters?.ToString() ?? "{}", Encoding.UTF8, "application/json");
            var task = httpClient.PostAsync($"{notionUrl}/api/v3/{method}", content);

            string result = task.Result.Content.ReadAsStringAsync().Result;
            return JObject.Parse(result);
        }
        private static void DumpObjects(dynamic obj)
        {
            foreach (JProperty property in (obj as JObject).Properties())
            {
                string table = property.Name;
                JObject objects = property.Value as JObject;

                foreach (JProperty p in objects.Properties())
                {
                    string id = p.Name;
                    dynamic o = p.Value as JObject;

                    DumpObject(table, id, o.value);
                }
            }
        }
        private static void DumpObject(string table, string id, dynamic obj)
        {
            string path = Path.Combine(databaseDirectory, table, id + ".json");

            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            using (StreamWriter writer = new StreamWriter(path))
            {
                string json = JsonConvert.SerializeObject(obj, new JsonSerializerSettings() { Formatting = Formatting.Indented });
                writer.WriteLine(json);
            }
        }
    }
}
