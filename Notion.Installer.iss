#ifndef SourceDirectory
  #define SourceDirectory "[Output]"
#endif

#define AppName "Notion"
#define AppPublisher "Notion Labs, Incorporated"
#define AppURL "https://notion.thedju.net/"
#define AppExeName "Notion.exe"

#define BuildVersion GetStringFileInfo(SourceDirectory + "\" + AppExeName, "ProductVersion")

[Setup]
AppId={{0E8E3A08-9A83-4BED-93BB-5A52408194D5}
AppName={#AppName}
AppVersion={#BuildVersion}
AppVerName={#AppName} {#BuildVersion} (thedju.net)
AppPublisher={#AppPublisher}
AppPublisherURL={#AppURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}
DefaultDirName={pf}\{#AppName}
DisableProgramGroupPage=yes
DisableWelcomePage=no
DisableDirPage=no
Compression=lzma
SolidCompression=yes
OutputBaseFilename={#AppName}-setup-v{#BuildVersion}
UninstallDisplayIcon={app}\{#AppExeName}
OutputDir=[Installer]
ArchitecturesInstallIn64BitMode=x64

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "{#SourceDirectory}\{#AppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDirectory}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{commonprograms}\{#AppName}"; Filename: "{app}\{#AppExeName}"
Name: "{commondesktop}\{#AppName}"; Filename: "{app}\{#AppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#AppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(AppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

