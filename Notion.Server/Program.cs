﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Notion
{
    public class Program
    {
        public static Dictionary<string, string> Options { get; private set; }
        public static string[] Parameters { get; private set; }

        public static ushort ServerPort { get; private set; } = 80;

        public static void Main(string[] args)
        {
            Options = args.Where(a => a.StartsWith("/"))
                          .Select(a => a.TrimStart('/'))
                          .Select(a => new { Parameter = a.Trim(), Separator = a.Trim().IndexOf(':') })
                          .ToDictionary(a => a.Separator == -1 ? a.Parameter : a.Parameter.Substring(0, a.Separator).ToLower(), a => a.Separator == -1 ? null : a.Parameter.Substring(a.Separator + 1), StringComparer.InvariantCultureIgnoreCase);
            Parameters = args.Where(a => !a.StartsWith("/"))
                             .ToArray();

            // Wipe everything if asked
            if (Options.ContainsKey("wipe"))
            {
                string[] wipedDirectories = new[] { "Database", "Authentication", "Files" };

                foreach (string wipedDirectory in wipedDirectories)
                {
                    foreach (string file in Directory.GetFiles(wipedDirectory))
                        File.Delete(file);

                    foreach (string directory in Directory.GetDirectories(wipedDirectory))
                    {
                        if (directory.EndsWith(".git"))
                            continue;

                        Directory.Delete(directory, true);
                    }
                }
            }

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            string serverPortText = Environment.GetEnvironmentVariable("NOTION_SERVER_PORT");
            if (!string.IsNullOrEmpty(serverPortText) && ushort.TryParse(serverPortText, out ushort serverPort))
                ServerPort = serverPort;

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseUrls($"http://*:{ServerPort}")
                .ConfigureAppConfiguration((hostingContext, configurationBuilder) =>
                {
                    var environment = hostingContext.HostingEnvironment;
                    configurationBuilder.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                })
                .UseConfiguration(configuration)
                .UseStartup<Startup>()
                .Build();
        }
    }
}
