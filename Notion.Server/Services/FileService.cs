﻿using System.Collections.Generic;
using System.IO;

namespace Notion.Server.Services
{
    public class PendingFileUpload
    {
        public dynamic User { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }

    public class FileService
    {
        private readonly DatabaseService database;
        private readonly string directory = "Files";

        private Dictionary<string, PendingFileUpload> pendingFileUploads = new Dictionary<string, PendingFileUpload>();

        public FileService(DatabaseService database, bool resetUsers = false)
        {
            this.database = database;

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        public string GetUploadSlot(dynamic user, string name)
        {
            string originalName = name;
            string path = GetPath(user, originalName);

            int counter = 1;

            while (File.Exists(path))
            {
                counter++;

                name = (Path.GetFileNameWithoutExtension(originalName) + "-" + counter + Path.GetExtension(originalName)).TrimStart('-');
                path = GetPath(user, name);
            }

            return name;
        }

        public void UploadFile(dynamic user, string name, Stream stream)
        {
            string path = GetPath(user, name);
            string parent = Path.GetDirectoryName(path);

            if (!Directory.Exists(parent))
                Directory.CreateDirectory(parent);

            using (FileStream fileStream = File.Open(path, FileMode.Create))
                stream.CopyTo(fileStream);
        }
        public Stream DownloadFile(dynamic user, string name)
        {
            string path = GetPath(user, name);

            try
            {
                return File.OpenRead(path);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (DirectoryNotFoundException)
            {
                return null;
            }
        }

        private string GetPath(dynamic user, string name)
        {
            return Path.Combine(directory, (string)user.id, name);
        }
    }
}