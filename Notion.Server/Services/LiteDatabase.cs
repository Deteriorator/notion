﻿using System.Collections.Generic;
using System.Linq;

using LiteDB;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Notion.Server.Model
{
    public class LiteDatabase : Database
	{
        public override IEnumerable<User> Users => users;
        public override IEnumerable<UserRoot> UserRoots => userRoots;
        public override IEnumerable<Space> Spaces => spaces;
        public override IEnumerable<SpaceView> SpaceViews => spaceViews;
        public override IEnumerable<Block> Blocks => blocks;
        public override IEnumerable<Collection> Collections => collections;
        public override IEnumerable<CollectionView> CollectionViews => collectionViews;

        private List<User> users = new List<User>();
        private List<UserRoot> userRoots = new List<UserRoot>();
        private List<Space> spaces = new List<Space>();
        private List<SpaceView> spaceViews = new List<SpaceView>();
        private List<Block> blocks = new List<Block>();
        private List<Collection> collections = new List<Collection>();
        private List<CollectionView> collectionViews = new List<CollectionView>();

        private LiteDB.LiteDatabase database;
        private LiteCollection<BsonDocument> userCollection;
        private LiteCollection<BsonDocument> userRootCollection;
        private LiteCollection<BsonDocument> spaceCollection;
        private LiteCollection<BsonDocument> spaceViewCollection;
        private LiteCollection<BsonDocument> blockCollection;
        private LiteCollection<BsonDocument> collectionCollection;
        private LiteCollection<BsonDocument> collectionViewCollection;

        public LiteDatabase(string databasePath, bool createNew = false)
        {
            database = new LiteDB.LiteDatabase(databasePath);

            userCollection = database.GetCollection("users");
            userRootCollection = database.GetCollection("user_roots");
            spaceCollection = database.GetCollection("spaces");
            spaceViewCollection = database.GetCollection("space_views");
            blockCollection = database.GetCollection("blocks");
            collectionCollection = database.GetCollection("collections");
            collectionViewCollection = database.GetCollection("collection_views");

            if (createNew)
                Clear();

            Refresh();
        }

        public override void Clear()
        {
            database.DropCollection(userCollection.Name);
            database.DropCollection(userRootCollection.Name);
            database.DropCollection(spaceCollection.Name);
            database.DropCollection(spaceViewCollection.Name);
            database.DropCollection(blockCollection.Name);
            database.DropCollection(collectionCollection.Name);
            database.DropCollection(collectionViewCollection.Name);

            Block[] blocks = Templates.LoadTemplates();
            foreach (Block block in blocks)
                AddBlock(block);
        }
        public override void Refresh()
        {
            users = userCollection.FindAll().Select(Deserialize<User>).ToList();
            userRoots = userRootCollection.FindAll().Select(Deserialize<UserRoot>).ToList();
            spaces = spaceCollection.FindAll().Select(Deserialize<Space>).ToList();
            spaceViews = spaceViewCollection.FindAll().Select(Deserialize<SpaceView>).ToList();
            blocks = blockCollection.FindAll().Select(Deserialize<Block>).ToList();
            collections = collectionCollection.FindAll().Select(Deserialize<Collection>).ToList();
            collectionViews = collectionViewCollection.FindAll().Select(Deserialize<CollectionView>).ToList();
        }

        public override User AddUser(User user)
        {
            BsonDocument bson = Serialize(user);

            userCollection.Insert(bson);
            users.Add(user);

            return user;
        }
        public override void UpdateUser(User user)
        {
            BsonDocument bson = Serialize(user);

            userCollection.Update(bson);

            users.RemoveAll(u => u.Id == user.Id);
            users.Add(user);
        }
        public override void RemoveUser(User user)
        {
            users.Remove(user);
            userCollection.Delete(user.Id);
        }

        public override UserRoot AddUserRoot(UserRoot userRoot)
        {
            BsonDocument bson = Serialize(userRoot);

            userRootCollection.Insert(bson);
            userRoots.Add(userRoot);

            return userRoot;
        }
        public override void UpdateUserRoot(UserRoot userRoot)
        {
            BsonDocument bson = Serialize(userRoot);

            userRootCollection.Update(bson);

            userRoots.RemoveAll(u => u.Id == userRoot.Id);
            userRoots.Add(userRoot);
        }
        public override void RemoveUserRoot(UserRoot userRoot)
        {
            userRoots.Remove(userRoot);
            userRootCollection.Delete(userRoot.Id);
        }

        public override Space AddSpace(Space space)
        {
            BsonDocument bson = Serialize(space);

            spaceCollection.Insert(bson);
            spaces.Add(space);

            return space;
        }
        public override void UpdateSpace(Space space)
        {
            BsonDocument bson = Serialize(space);

            spaceCollection.Update(bson);

            spaces.RemoveAll(s => s.Id == space.Id);
            spaces.Add(space);
        }
        public override void RemoveSpace(Space space)
        {
            spaces.Remove(space);
            spaceCollection.Delete(space.Id);
        }

        public override SpaceView AddSpaceView(SpaceView spaceView)
        {
            BsonDocument bson = Serialize(spaceView);

            spaceViewCollection.Insert(bson);
            spaceViews.Add(spaceView);

            return spaceView;
        }
        public override void UpdateSpaceView(SpaceView spaceView)
        {
            BsonDocument bson = Serialize(spaceView);

            spaceViewCollection.Update(bson);

            spaceViews.RemoveAll(s => s.Id == spaceView.Id);
            spaceViews.Add(spaceView);
        }
        public override void RemoveSpaceView(SpaceView spaceView)
        {
            spaceViews.Remove(spaceView);
            spaceViewCollection.Delete(spaceView.Id);
        }

        public override Block AddBlock(Block block)
        {
            BsonDocument bson = Serialize(block);

            blockCollection.Insert(bson);
            blocks.Add(block);

            return block;
        }
        public override void UpdateBlock(Block block)
        {
            BsonDocument bson = Serialize(block);

            blockCollection.Update(bson);

            blocks.RemoveAll(b => b.Id == block.Id);
            blocks.Add(block);
        }
        public override void RemoveBlock(Block block)
        {
            blocks.Remove(block);
            blockCollection.Delete(block.Id);
        }

        public override Collection AddCollection(Collection collection)
        {
            BsonDocument bson = Serialize(collection);

            collectionCollection.Insert(bson);
            collections.Add(collection);

            return collection;
        }
        public override void UpdateCollection(Collection collection)
        {
            BsonDocument bson = Serialize(collection);

            collectionCollection.Update(bson);

            collections.RemoveAll(c => c.Id == collection.Id);
            collections.Add(collection);
        }
        public override void RemoveCollection(Collection collection)
        {
            collections.Remove(collection);
            collectionCollection.Delete(collection.Id);
        }

        public override CollectionView AddCollectionView(CollectionView collectionView)
        {
            BsonDocument bson = Serialize(collectionView);

            collectionViewCollection.Insert(bson);
            collectionViews.Add(collectionView);

            return collectionView;
        }
        public override void UpdateCollectionView(CollectionView collectionView)
        {
            BsonDocument bson = Serialize(collectionView);

            collectionViewCollection.Update(bson);

            collectionViews.RemoveAll(c => c.Id == collectionView.Id);
            collectionViews.Add(collectionView);
        }
        public override void RemoveCollectionView(CollectionView collectionView)
        {
            collectionViews.Remove(collectionView);
            collectionViewCollection.Delete(collectionView.Id);
        }

        private BsonDocument Serialize(object obj)
        {
            Dictionary<string, long> bigNumbers = new Dictionary<string, long>();
            JObject jObject = JObject.FromObject(obj);

            foreach (JProperty property in jObject.Properties().ToArray())
            {
                if (property.Value.Type == JTokenType.Integer)
                {
                    long value = property.Value.Value<long>();
                    if (value > int.MaxValue || value < int.MinValue)
                    {
                        bigNumbers.Add(property.Name, value);
                        jObject.Remove(property.Name);
                    }
                }
            }

            string json = JsonConvert.SerializeObject(jObject);
            BsonDocument bson = LiteDB.JsonSerializer.Deserialize(json) as BsonDocument;

            foreach (var bigNumber in bigNumbers)
                bson.Add(bigNumber.Key, new BsonValue(bigNumber.Value));

            BsonValue id = bson["id"];
            if (id != null)
            {
                bson.Add("_id", id);
                bson.Remove("id");
            }

            return bson;
        }
        private T Deserialize<T>(BsonDocument bson)
        {
            Dictionary<string, long> bigNumbers = new Dictionary<string, long>();

            BsonValue id = bson["_id"];
            if (id != null)
            {
                bson.Add("id", id);
                bson.Remove("_id");
            }

            foreach (var property in bson.ToArray())
            {
                if (property.Value.Type == BsonType.Int64)
                {
                    bigNumbers.Add(property.Key, property.Value.AsInt64);
                    bson.Remove(property.Key);
                }
            }

            string json = LiteDB.JsonSerializer.Serialize(bson);
            JObject jObject = JObject.Parse(json);

            foreach (var bigNumber in bigNumbers)
                jObject[bigNumber.Key] = bigNumber.Value;

            T obj = jObject.ToObject<T>();

            foreach (var bigNumber in bigNumbers)
                bson.Add(bigNumber.Key, new BsonValue(bigNumber.Value));

            return obj;
        }
    }
}