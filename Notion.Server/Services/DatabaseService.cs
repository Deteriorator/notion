﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Notion.Server.Services
{
    public abstract class DatabaseService
	{
        public abstract class Collection<T> : IEnumerable<T>
        {
            public DatabaseService Database { get; }
            public string Name { get; }

            protected ConcurrentDictionary<string, T> objects = new ConcurrentDictionary<string, T>();

            public Collection(DatabaseService database, string name)
            {
                Database = database;
                Name = name;
            }
            
            public IEnumerator<T> GetEnumerator() => objects.Values.GetEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => objects.Values.GetEnumerator();
        }
        public class Collection : Collection<dynamic>
        {
            public Collection(DatabaseService database, string name) : base(database, name) { }

            public dynamic Find(string id)
            {
                if (objects.TryGetValue(id, out dynamic obj))
                    return obj;

                return null;
            }
            public dynamic[] FindAll(params string[] ids)
            {
                return ids.Select(Find).ToArray();
            }

            public void Add(dynamic obj)
            {
                objects.TryAdd((string)obj.id, obj);

                Database.OnAdd(Name, obj);
            }
            internal void AddInternal(dynamic obj)
            {
                objects.TryAdd((string)obj.id, obj);
            }
            public void Update(dynamic obj)
            {
                objects.TryRemove((string)obj.id, out dynamic _);
                objects.TryAdd((string)obj.id, obj);

                Database.OnUpdate(Name, obj);
            }
            public void Remove(dynamic obj)
            {
                objects.TryRemove((string)obj.id, out dynamic _);

                Database.OnRemove(Name, obj);
            }
        }

        public const string UserType = "notion_user";
        public const string UserRootType = "user_root";
        public const string SpaceType = "space";
        public const string SpaceViewType = "space_view";
        public const string BlockType = "block";
        public const string CollectionType = "collection";
        public const string CollectionViewType = "collection_view";

        public abstract Collection this[string type] { get; }

        public Collection Users => this[UserType];
        public Collection UserRoots => this[UserRootType];
        public Collection Spaces => this[SpaceType];
        public Collection SpaceViews => this[SpaceViewType];
        public Collection Blocks => this[BlockType];
        public Collection Collections => this[CollectionType];
        public Collection CollectionViews => this[CollectionViewType];

        public abstract void Clear();
        public abstract void Refresh();

        protected abstract void OnAdd(string type, dynamic value);
        protected abstract void OnUpdate(string type, dynamic value);
        protected abstract void OnRemove(string type, dynamic value);

        public void LoadTemplates()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string prefix = typeof(DatabaseService).Namespace + ".Templates.";

            string[] resources = assembly.GetManifestResourceNames()
                .Where(r => r.StartsWith(prefix))
                .ToArray();

            List<dynamic> blocks = new List<dynamic>();

            Parallel.ForEach(resources, resource =>
            {
                using (Stream templateStream = assembly.GetManifestResourceStream(resource))
                using (StreamReader templateReader = new StreamReader(templateStream))
                {
                    string template = templateReader.ReadToEnd();
                    dynamic obj = JObject.Parse(template);

                    foreach (JProperty property in obj.subtreeRecordMap.Properties())
                    {
                        string type = property.Name;
                        JObject objects = property.Value as JObject;

                        foreach (dynamic item in objects.Properties())
                        {
                            dynamic value = item.Value.value;
                            this[type].Update(value);
                        }
                    }
                }
            });
        }

#if DEBUG
        public void PopulateDummyData()
        {
            Clear();

            {
                string json = @"{
                    ""id"": ""5c2b881b-32e1-44f2-8d48-23233675a465"",
                    ""version"": 1,
                    ""email"": ""julien.batonnet@gmail.com"",
                    ""given_name"": ""Julien"",
                    ""family_name"": ""Batonnet"",
                    ""profile_photo"": ""https://lh5.googleusercontent.com/-OB0UhpuHtek/AAAAAAAAAAI/AAAAAAAAA4c/my2Ul8IDsBc/photo.jpg?sz=50"",
                    ""onboarding_completed"": true,
                    ""mobile_onboarding_completed"": true,
                    ""persona"": ""programmer"",
                    ""type"": ""personally"",
                    ""time_zone"": ""America/Toronto"",
                    ""locale"": ""fr"",
                    ""disable_analytics"": true
                }";

                dynamic obj = JObject.Parse(json);
                Users.Add(obj);
            }

            {
                string json = @"{
                    ""id"": ""5c2b881b-32e1-44f2-8d48-23233675a465"",
                    ""version"": 1,
                    ""space_views"": [
                        ""e0fb49d9-54eb-45f5-b05f-dc24a037a3c1""
                    ]
                }";

                dynamic obj = JObject.Parse(json);
                UserRoots.Add(obj);
            }

            {
                string json = @"{
                    ""id"": ""d3292005-fd35-4482-8ff4-18237b5d897f"",
                    ""version"": 1,
                    ""name"": ""Julien :)"",
                    ""domain"": ""dju"",
                    ""permissions"": [
                        {
                            ""role"": ""editor"",
                            ""type"": ""user_permission"",
                            ""user_id"": ""5c2b881b-32e1-44f2-8d48-23233675a465""
                        }
                    ],
                    ""pages"": [
                        ""c36fb5c6-456d-49fb-95b2-ac2f60454677""
                    ],
                    ""icon"": ""/file/dju.jpg"",
                    ""beta_enabled"": false
                }";

                dynamic obj = JObject.Parse(json);
                Spaces.Add(obj);
            }

            {
                string json = @"{
                    ""id"": ""e0fb49d9-54eb-45f5-b05f-dc24a037a3c1"",
                    ""version"": 1,
                    ""space_id"": ""d3292005-fd35-4482-8ff4-18237b5d897f"",
                    ""visited_templates"": [],
                    ""sidebar_hidden_templates"": [
                        ""88c9c2b0-d732-4342-8963-0580a4725571"",
                        ""6c02254d-2c59-4932-8074-029480665065"",
                        ""ba03136b-703a-4e41-bc58-12d0b71f12d4"",
                        ""ebc30c0e-3e45-4a4d-8be2-8c5246dbdc66""
                    ],
                    ""notify_mobile"": true,
                    ""notify_desktop"": true,
                    ""notify_email"": true,
                    ""parent_id"": ""5c2b881b-32e1-44f2-8d48-23233675a465"",
                    ""parent_table"": ""user_root"",
                    ""alive"": true
                }";

                dynamic obj = JObject.Parse(json);
                SpaceViews.Add(obj);
            }

            {
                string json = @"{
                    ""id"": ""c36fb5c6-456d-49fb-95b2-ac2f60454677"",
                    ""version"": 4,
                    ""type"": ""page"",
                    ""created_by"": ""5c2b881b-32e1-44f2-8d48-23233675a465"",
                    ""created_time"": 1530156890616,
                    ""last_edited_by"": ""5c2b881b-32e1-44f2-8d48-23233675a465"",
                    ""last_edited_time"": 1530156890616,
                    ""parent_id"": ""d3292005-fd35-4482-8ff4-18237b5d897f"",
                    ""parent_table"": ""space"",
                    ""alive"": true
                }";

                dynamic obj = JObject.Parse(json);
                Blocks.Add(obj);
            }

            Refresh();
        }
#endif
    }
}