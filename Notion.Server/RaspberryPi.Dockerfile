FROM microsoft/dotnet:2.2-aspnetcore-runtime-stretch-slim-arm32v7 AS base
WORKDIR /app

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY . .
RUN dotnet restore -nowarn:msb3202,nu1503
RUN dotnet build -c Debug -o /app

FROM build AS publish
RUN dotnet publish -c Debug -r linux-arm -o /app
RUN rm -rf /app/Database

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
VOLUME /app/Database
EXPOSE 80
CMD ["./Notion.Server"]
