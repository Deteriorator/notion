﻿using System.IO;
using System.Net;

using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Notion.Server.Controllers;
using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class FileController : BaseController
    {
        private const string notionPrefix = "https://www.notion.so";

        private readonly DatabaseService database;
        private readonly AuthenticationService authentication;
        private readonly FileService file;

        public FileController(DatabaseService database, AuthenticationService authentication, FileService file) : base(database)
        {
            this.database = database;
            this.authentication = authentication;
            this.file = file;
        }

        [HttpGet("/files/{url}")]
        [HttpGet("/image/{url}")]
        public IActionResult Forward(string url)
        {
            url = WebUtility.UrlDecode(url);

            if (url.StartsWith(notionPrefix))
                url = url.Substring(notionPrefix.Length);

            return Redirect(url);
        }

        [HttpGet("/storage/{userId}/{name}")]
        public IActionResult Download(string userId, string name)
        {
            dynamic currentUser = GetCurrentUser(authentication);
            dynamic fileAuthor = database.Users.Find(userId);

            Stream stream = file.DownloadFile(fileAuthor, name);
            if (stream == null)
                return NotFound();

            return File(stream, "application/octet-stream");
        }

        [HttpOptions("/storage/{userId}/{name}")]
        public IActionResult GetOptions(string userId, string name)
        {
            return Ok();
        }

        [HttpPut("/storage/{userId}/{name}")]
        public IActionResult Upload(string userId, string name)
        {
            dynamic currentUser = GetCurrentUser(authentication);

            if (currentUser.id != userId)
            {
                JObject error = new JObject()
                {
                    { "message", "Forbidden." }
                };

                return BadRequest(error);
            }

            file.UploadFile(currentUser, name, Request.Body);
            return Ok();
        }
    }
}
