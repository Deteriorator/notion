﻿using System;
using System.Linq;
using System.Net;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

using Newtonsoft.Json.Linq;

using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class AuthController : BaseController
    {
        private readonly DatabaseService database;
        private readonly AuthenticationService authentication;

        private readonly bool enableAutomaticRegistration;
        private readonly string[] allowedRegistrations;

        public AuthController(DatabaseService database, AuthenticationService authentication) : base(database)
        {
            this.database = database;
            this.authentication = authentication;

            string enableAutomaticRegistrationText = Environment.GetEnvironmentVariable("NOTION_AUTH_AUTOREGISTRATION");
            enableAutomaticRegistration = enableAutomaticRegistrationText?.ToLower() == "true";

            string allowedRegistrationsText = Environment.GetEnvironmentVariable("NOTION_AUTH_ALLOWEDREGISTRATIONS");
            if (!string.IsNullOrEmpty(allowedRegistrationsText))
            {
                allowedRegistrations = allowedRegistrationsText
                    .ToLower()
                    .Split(new[] { ',', ';', ' ', '|' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        [HttpGet("/oauth2callback")]
        public IActionResult Callback()
        {
            string queryString = Request.QueryString.Value;
            var queryDictionary = QueryHelpers.ParseQuery(queryString);

            if (!queryDictionary.TryGetValue("state", out var stateValues))
                return BadRequest();
            if (!queryDictionary.TryGetValue("code", out var codeValues))
                return BadRequest("/");

            string state = WebUtility.UrlDecode(stateValues[0]);
            string code = WebUtility.UrlDecode(codeValues[0]);

            string clientId = ClientId;
            if (!authentication.ValidateAuthenticationCallback(clientId, state))
                return BadRequest();

            string baseUrl = BaseUrl;
            dynamic token = authentication.GetTokenFromCode(baseUrl, code);
            if (token == null)
                return null;

            dynamic userProfile = authentication.GetUserProfile((string)token.token_type, (string)token.access_token);
            if (userProfile == null)
                return null;

            string email = userProfile.email;
            bool isNewSignup = false;
            
            dynamic user = database.Users.FirstOrDefault(u => u.email == email);
            if (user == null)
            {
                bool allowedRegistration = enableAutomaticRegistration || (allowedRegistrations != null && allowedRegistrations.Contains(email.ToLower()));
                if (allowedRegistration)
                {
                    user = new JObject()
                    {
                        { "id", Guid.NewGuid().ToString("D") },
                        { "version", 1 },
                        { "given_name", userProfile.given_name },
                        { "family_name", userProfile.family_name },
                        { "profile_photo", userProfile.picture },
                        { "email", userProfile.email },
                        { "type", "personally" },
                        { "persona", "programmer" },
                        { "onboarding_completed", false },
                        { "mobile_onboarding_completed", false }
                    };
                    database.Users.Add(user);

                    /*dynamic space = new JObject()
                    {
                        { "id", Guid.NewGuid().ToString("D") },
                        { "version", 1 },
                        { "name", "My space" },
                        { "domain", "space-" + user.GetHashCode() },
                        { "permissions", new JArray() { new JObject {
                            { "role", "editor" },
                            { "type", "user_permission" },
                            { "user_id", user.id },
                        } } }
                    };
                    database.Spaces.Add(space);

                    dynamic spaceView = new JObject()
                    {
                        { "id", Guid.NewGuid().ToString("D") },
                        { "version", 1 },
                        { "space_id", space.id },
                        { "parent_id", user.id },
                        { "parent_table", "user_root" },
                        { "alive", true },
                        { "notify_mobile", false },
                        { "notify_desktop", false },
                        { "notify_email", false },
                    };
                    database.SpaceViews.Add(spaceView);*/

                    dynamic userRoot = new JObject()
                    {
                        { "id", user.id },
                        { "version", 1 },
                        { "space_views", new JArray(new string[] { /*spaceView.id*/ }) }
                    };
                    database.UserRoots.Add(userRoot);

                    // FIXME: Implement correct onboarding
                    isNewSignup = true;
                }
                else
                    return Redirect("/");
            }
               
            AuthenticatedUser authenticatedUser = authentication.AuthenticateUser(clientId, user);
            if (authenticatedUser == null)
                return BadRequest();

            Response.Cookies.Append("token", authenticatedUser.Token);
            Response.Cookies.Append("userId", authenticatedUser.UserId);

            return Redirect("/logincallback?isNewSignup=" + isNewSignup.ToString().ToLower());
        }

        [HttpGet("/server/logout")]
        public IActionResult Logout()
        {
            authentication.LogoutUser(ClientId, Token, UserId);

            Response.Cookies.Delete("token");
            Response.Cookies.Delete("userId");

            return Redirect("/logoutcallback");
        }
    }
}
