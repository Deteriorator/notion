﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Notion.Server.Helpers;
using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public abstract class BaseController : Controller
    {
        public string BaseUrl => UrlHelper.GetBaseUrl(Request);

        public string ClientId
        {
            get
            {
                string clientId = Request.Cookies["__cfduid"];

                if (clientId == null)
                {
                    clientId = Guid.NewGuid().ToString("D");
                    Response.Cookies.Append("__cfduid", clientId);
                }

                return clientId;
            }
        }
        public string Token
        {
            get => Request.Cookies["token"];
            set
            {
                if (value == null)
                    Response.Cookies.Delete("token");
                else
                    Response.Cookies.Append("token", value);
            }
        }
        public string UserId
        {
            get => Request.Cookies["userId"];
            set
            {
                if (value == null)
                    Response.Cookies.Delete("userId");
                else
                    Response.Cookies.Append("userId", value);
            }
        }

        private readonly DatabaseService database;

        public BaseController(DatabaseService database)
        {
            this.database = database;
        }

        public dynamic GetParameters()
        {
            using (StreamReader bodyReader = new StreamReader(Request.Body))
            using (JsonTextReader jsonReader = new JsonTextReader(bodyReader))
            {
                return JObject.Load(jsonReader);
            }
        }
        public dynamic GetCurrentUser(AuthenticationService authentication)
        {
            string clientId = ClientId;
            string token = Token;
            string userId = UserId;

            if (clientId == null || token == null || userId == null)
                return null;

            return authentication.CheckAndGetUser(clientId, token, userId);
        }

        public int GetRoleLevel(string role) => NotionHelper.GetRoleLevel(role);
        public dynamic GetPermission(dynamic obj, dynamic user) => NotionHelper.GetPermission(database, obj, user);
        public dynamic StripPermissions(dynamic obj) => NotionHelper.StripPermissions(obj);

        public JProperty WrapObject(dynamic obj, dynamic element) => NotionHelper.WrapObject(database, obj, element);
    }
}
