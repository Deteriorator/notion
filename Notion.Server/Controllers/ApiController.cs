﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class ApiController : BaseController
    {
        private readonly DatabaseService database;
        private readonly AuthenticationService authenticationService;
        private readonly FileService fileService;
        private readonly TaskService taskService;

        private Dictionary<string, Func<object>> publicMethods;
        private Dictionary<string, Func<dynamic, object>> privateMethods;

        public ApiController(DatabaseService database, AuthenticationService authenticationService, FileService fileService, TaskService taskService) : base(database)
        {
            this.database = database;
            this.authenticationService = authenticationService;
            this.fileService = fileService;
            this.taskService = taskService;

            this.publicMethods = typeof(ApiController)
                .GetMethods(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance)
                .Where(m => !m.IsConstructor && m.GetParameters().Length == 0)
                .ToDictionary(m => m.Name.ToLower(), m => m.CreateDelegate(typeof(Func<object>), this) as Func<object>);

            this.privateMethods = typeof(ApiController)
                .GetMethods(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance)
                .Where(m => !m.IsConstructor && m.GetParameters().FirstOrDefault()?.Name == "user")
                .ToDictionary(m => m.Name.ToLower(), m => m.CreateDelegate(typeof(Func<dynamic, object>), this) as Func<dynamic, object>);
        }

        [HttpPost("api/v3/{name}")]
        public object Call(string name)
        {
            string methodName = name.ToLower();

            // Try to find a public method
            if (publicMethods.TryGetValue(name.ToLower(), out Func<object> publicMethod))
                return publicMethod();

            // Check the current user for private methods
            dynamic user = GetCurrentUser(authenticationService);
            if (user == null)
            {
                JObject error = new JObject()
                {
                    { "message", "Must be authenticated." }
                };

                return BadRequest(error);
            }

            // Forward to the right endpoint
            if (privateMethods.TryGetValue(name.ToLower(), out Func<dynamic, object> privateMethod))
                return privateMethod(user);

            return NotFound();
        }

        // Finished

        public object Ping()
        {
            return new JObject();
        }
        public object SubmitTransaction(dynamic user)
        {
            dynamic parameters = this.GetParameters();

            foreach (dynamic operation in parameters.operations)
            {
                string id = operation.id;
                string command = operation.command;
                string table = operation.table;
                string[] path = operation.path.ToObject<string[]>();
                dynamic args = operation.args;

                dynamic source = database[table].FirstOrDefault(o => o.id == id);

                if (source == null)
                {
                    source = new JObject() {
                        { "id", id }
                    };

                    database[table].Add(source);
                }

                if (source == null)
                    continue;

                string json = source.ToString();
                JObject target = JObject.Parse(json);

                switch (command)
                {
                    case "set":
                    case "update":
                        {
                            if (args is JObject argsObject)
                            {
                                JProperty property = target.FindProperty(path);

                                if (property.Value == null || property.Value.Type != JTokenType.Object)
                                    property.Value = new JObject();

                                JObject obj = property.Value as JObject;

                                foreach (JProperty arg in argsObject.Properties())
                                    obj[arg.Name] = arg.Value;
                            }
                            else
                            {
                                JProperty property = target.FindProperty(path);
                                property.Value = args;
                            }
                        }
                        break;

                    case "listBefore":
                        {
                            JProperty property = target.FindProperty(path);

                            if (property.Value == null || property.Value.Type != JTokenType.Array)
                                property.Value = new JArray();

                            JArray array = property.Value as JArray;
                            List<string> values = array.ToObject<List<string>>();

                            string insertId = args.id;

                            if (args.before != null)
                            {
                                string beforeId = args.before;
                                int index = values.IndexOf(beforeId);

                                if (index == -1)
                                    array.Insert(0, insertId);
                                else
                                    array.Insert(index, insertId);
                            }
                            else
                                array.Insert(0, insertId);
                        }
                        break;

                    case "listAfter":
                        {
                            JProperty property = target.FindProperty(path);

                            if (property.Value == null || property.Value.Type != JTokenType.Array)
                                property.Value = new JArray();

                            JArray array = property.Value as JArray;
                            List<string> values = array.ToObject<List<string>>();

                            string insertId = args.id;

                            if (args.after != null)
                            {
                                string afterId = args.after;
                                int index = values.IndexOf(afterId);

                                if (index == -1)
                                    array.Add(insertId);
                                else
                                    array.Insert(index + 1, insertId);
                            }
                            else
                                array.Add(insertId);
                        }
                        break;

                    case "listRemove":
                        {
                            JProperty property = target.FindProperty(path);

                            if (property.Value == null || property.Value.Type != JTokenType.Array)
                                property.Value = new JArray();

                            JArray array = property.Value as JArray;
                            List<string> values = array.ToObject<List<string>>();

                            string removeId = args.id;
                            values.Remove(removeId);

                            property.Value = new JArray(values);
                        }
                        break;

                    case "setPermissionItem":
                        {
                            JArray permissions = target["permissions"] as JArray;
                            if (permissions == null)
                                target["permissions"] = permissions = new JArray();

                            dynamic currentPermission = null;

                            switch ((string)args.type)
                            {
                                case "public_permission":
                                    currentPermission = permissions.FirstOrDefault(p => p["type"]?.Value<string>() == "public_permission");
                                    break;

                                case "space_permission":
                                    currentPermission = permissions.FirstOrDefault(p => p["type"]?.Value<string>() == "space_permission");
                                    break;

                                case "user_permission":
                                    string userId = args.user_id;
                                    currentPermission = permissions.FirstOrDefault(p => p["type"]?.Value<string>() == "user_permission" && p["user_id"]?.Value<string>() == userId);
                                    break;

                                case "group_permission":
                                    string groupId = args.group_id;
                                    currentPermission = permissions.FirstOrDefault(p => p["type"]?.Value<string>() == "group_permission" && p["group_id"]?.Value<string>() == groupId);
                                    break;

                                default:
                                    Debugger.Break();
                                    break;
                            }

                            if (currentPermission != null)
                                permissions.Remove(currentPermission);
                            if (args.role != "none")
                                permissions.Add(args);
                        }
                        break;

                    default:
                        Debugger.Break();
                        break;
                }

                database[table].Update(target);
            }

            return new JObject();
        }
        public object GetGoogleAuthURL()
        {
            string authenticationUrl = authenticationService.GetAuthenticationURL(ClientId, BaseUrl);

            return new JObject()
            {
                { "url", authenticationUrl }
            };
        }
        public object GetUserAnalyticsSettings()
        {
            dynamic user = this.GetCurrentUser(authenticationService);

            dynamic result = new JObject()
            {
                { "isFullStoryEnabled", false },
                { "isIntercomEnabled", false },
                { "isAmplitudeEnabled", false }
            };

            if (user != null)
            {
                result.user_id = user.id;
                result.user_email = user.email;
            }

            return result;
        }
        public object LoadUserContent(dynamic user)
        {
            dynamic userRoot = database.UserRoots.Find((string)user.id);

            dynamic[] spaces = database.Spaces
                .Where(s => GetPermission(s, user) != null)
                .ToArray();

            dynamic[] spaceViews = database.SpaceViews
                .Where(s => s.parent_table == DatabaseService.UserRootType && s.parent_id == user.id)
                .ToArray();

            dynamic[] pages = database.Blocks
                .Where(b => b.type == "page")
                .Where(b => spaces.Any(s => s.id == b.parent_id))
                .ToArray();

            JObject result = new JObject()
            {
                { "recordMap", new JObject() {
                    { "notion_user", new JObject() {
                        WrapObject(user, "editor")
                    } },
                    { "user_root", new JObject() {
                        WrapObject(userRoot, "editor")
                    } },
                    { "space_view", new JObject(spaceViews.Select(s => WrapObject(s, "editor"))) },
                    { "space", new JObject(spaces.Select(s => WrapObject(s, user))) },
                    { "block", new JObject(pages.Select(p => WrapObject(p, "editor"))) }
                } }
            };

            return result;
        }
        public object GetRecordValues()
        {
            dynamic user = GetCurrentUser(authenticationService);
            dynamic parameters = GetParameters();

            JArray results;
            JObject result = new JObject()
            {
                { "results", results = new JArray() }
            };

            foreach (dynamic request in parameters.requests as JArray)
            {
                string table = request.table;
                string id = request.id;

                if (!Guid.TryParse(id, out Guid _))
                {
                    results.Add(new JObject() {
                        { "role", "editor" },
                        { "value", null }
                    });

                    continue;
                }

                dynamic obj = database[table].Find(id);
                dynamic permission = null;

                if (table == DatabaseService.BlockType || table == DatabaseService.SpaceType)
                {
                    permission = GetPermission(obj, user);

                    if (permission == null)
                    {
                        results.Add(new JObject() {
                            { "role", "editor" },
                            { "value", null }
                        });

                        continue;
                    }
                }

                results.Add(new JObject() {
                    { "role", permission?.role ?? "editor" },
                    { "value", obj }
                });
            }

            return result;
        }
        public object GetPublicPageData()
        {
            dynamic user = this.GetCurrentUser(authenticationService);
            dynamic parameters = this.GetParameters();

            string blockId = parameters.blockId;
            if (blockId == null)
                return new JObject();

            dynamic page = database.Blocks.Find(blockId);
            if (page == null)
                return new JObject();

            dynamic permission = GetPermission(page, user);
            JArray permissions = page.permissions as JArray;

            bool publicAccess = permissions != null && permissions.Any(p => (string)p["type"] == "public_permission" && (string)p["role"] != "none");
            bool explicitAccess = user != null && permissions != null && permissions.Any(p => (string)p["type"] == "user_permission" && (string)p["user_id"] != (string)user.id);

            dynamic space = page;
            while (space.parent_table != null)
                space = database[(string)space.parent_table].Find((string)space.parent_id);

            JObject result = new JObject()
            {
                { "spaceName", space.name },
                { "spaceId", space.id },
                { "domain", space.domain },
                { "canJoinSpace", true },
                { "userHasExplicitAccess", explicitAccess },
                { "hasPublicAccess", publicAccess }
            };

            if (space.icon != null)
                result["icon"] = space.icon;

            return result;
        }

        // In progress

        public object GetDesktopDownloadUrl()
        {
            var result = new JObject()
            {
                { "url", "http://www.notion.so" }
            };

            return result;
        }
        public object GetSubscriptionData(dynamic user)
        {
            dynamic parameters = this.GetParameters();
            string spaceId = parameters.spaceId;

            dynamic space = database.Spaces.FirstOrDefault(s => s.id == spaceId);

            JObject result = new JObject()
            {
                { "type", "subscribed_admin" },
                { "blockUsage", 128 },
                { "members", new JArray((space.permissions as JArray).Select(p => new JObject() {
                    { "userId", p["user_id"] },
                    { "role", p["role"] },
                    { "isGuest", false },
                    { "guestPageIds", new JArray() }
                })) },
                { "credits", new JArray() },
                { "totalCredit", 0 },
                { "availableCredit", 0 }
            };

            return result;
        }
        public object GetUserSharedPages(dynamic user)
        {
            dynamic[] spaces = database.Spaces
                .ToArray();

            dynamic[] pages = database.Blocks
                .Where(b => b.type == "page")
                .Where(b => spaces.Any(s => s.id == b.parent_id))
                .Where(b => GetPermission(b, user) != null)
                .ToArray();

            JObject result = new JObject()
            {
                { "pages", new JArray(pages.Select(p => new JObject() {
                    { "id", p.id },
                    { "spaceId", p.parent_id }
                })) },
                { "recordMap", new JObject() {
                    { "block", new JObject(pages.Select(p => new JProperty(
                        (string)p.id,
                        new JObject() {
                            { "role", "editor" },
                            { "value", p }
                        }
                    ))) },
                    { "space", new JObject(spaces.Select(s => new JProperty(
                        (string)s.id,
                        new JObject() {
                            { "role", "editor" },
                            { "value", s }
                        }
                    ))) }
                } }
            };

            return result;
        }
        public object LoadPageChunk()
        {
            dynamic user = GetCurrentUser(authenticationService);
            dynamic parameters = GetParameters();

            string pageId = parameters.pageId;
            dynamic page = database.Blocks.Find(pageId);

            List<dynamic> blocks = new List<dynamic>();

            if (page != null)
            {
                // List every related blocks
                dynamic[] pass = new[] { page };

                while (pass.Length > 0)
                {
                    blocks.AddRange(pass);

                    pass = pass
                        .SelectMany(b => b.content == null ? new string[0] : (b.content as JArray).Values<string>())
                        .Except(blocks.Select(b => (string)b.id))
                        .Select(database.Blocks.Find)
                        .Where(b => b != null)
                        .ToArray();
                }
            }

            dynamic permission = GetPermission(page, user);

            // Send the result
            JObject result = new JObject()
            {
                { "recordMap", new JObject() {
                    { "block", new JObject(blocks.Select(p => new JProperty(
                        (string)p.id,
                        new JObject() {
                            { "role", permission?.role ?? "editor" },
                            { "value", p }
                        }
                    ))) }
                } },
                { "cursor", new JObject() {
                    { "stack", new JArray() }
                } }
            };

            return result;
        }
        public object GetPublicSpaceData(dynamic user)
        {
            using (StreamReader bodyReader = new StreamReader(this.Request.Body))
            using (JsonTextReader jsonReader = new JsonTextReader(bodyReader))
            {
                JObject parameters = JObject.Load(jsonReader);

                JArray results;
                JObject result = new JObject()
                {
                    { "results", results = new JArray() }
                };

                JArray spaceIds = parameters["spaceIds"] as JArray;
                foreach (JToken spaceIdToken in spaceIds)
                {
                    string spaceId = spaceIdToken.Value<string>();
                    dynamic space = database.Spaces.FirstOrDefault(s => s.id == spaceId);

                    results.Add(new JObject() {
                        { "name", space.name },
                        { "domain", space.domain },
                        { "memberCount", (space.permissions as JArray).Count }
                    });
                }

                return result;
            }
        }
        public object LoadBlockSubtree()
        {
            dynamic user = this.GetCurrentUser(authenticationService);

            dynamic parameters = this.GetParameters();
            string blockId = parameters.blockId;

            dynamic block = database.Blocks.Find(blockId);
            if (block == null)
                return new JObject();

            bool authorized = GetPermission(block, user) != null;
            if (!authorized)
                return Unauthorized();

            // List every related objects
            List<dynamic> blocks = new List<dynamic>();
            List<dynamic> collections = new List<dynamic>();
            List<dynamic> collectionViews = new List<dynamic>();

            dynamic[] pass = new[] { block };

            while (pass.Length > 0)
            {
                dynamic[] currentPass = pass;

                blocks.AddRange(currentPass);

                // Process content
                pass = currentPass
                    .SelectMany<dynamic, string>(b => b.content?.ToObject<string[]>() ?? new string[0])
                    .Except(blocks.Select(b => (string)b.id))
                    .Select(database.Blocks.Find)
                    .ToArray();

                // Process collections
                foreach (dynamic b in currentPass)
                {
                    if (b.collection_id != null)
                    {
                        dynamic collection = database.Collections.Find((string)b.collection_id);
                        collections.Add(collection);

                        dynamic[] subCollectionViews = database.CollectionViews
                            .Where(c => c.parent_id == b.id)
                            .ToArray();

                        collectionViews.AddRange(subCollectionViews);

                        dynamic[] subBlocks = database.Blocks
                            .Where(c => c.parent_id == collection.id)
                            .ToArray();

                        pass = pass
                            .Concat(subBlocks)
                            .ToArray();
                    }
                }
            }

            // Send the result
            JObject result = new JObject()
            {
                { "subtreeRecordMap", new JObject() {
                    { "block", new JObject(blocks.Select(b => WrapObject(b, "editor"))) },
                    { "collection", new JObject(collections.Select(c => WrapObject(c, "editor"))) },
                    { "collection_view", new JObject(collectionViews.Select(c => WrapObject(c, "editor"))) },
                    { "space", new JObject() }
                } }
            };

            return result;
        }
        public object PingUserNotifications(dynamic user)
        {
            dynamic[] spaces = database.Spaces.ToArray();

            var result = new JObject()
            {
                { "results", new JArray(spaces.Select(s => new JObject() {
                    { "spaceId", s.id },
                    { "unread", new JObject() {
                        { "mentions", 0 },
                        { "nonMentions", 0 }
                    } },
                    { "unreceived", new JObject() {
                        { "notificationIds", new JArray() }
                    } }
                })) },
                { "recordMap", new JObject() }
            };

            return result;
        }
        public object QueryCollection(dynamic user)
        {
            dynamic parameters = this.GetParameters();

            string collectionId = parameters.collectionId;
            string collectionViewId = parameters.collectionViewId;

            dynamic collection = database.Collections.Find(collectionId);
            dynamic collectionView = database.CollectionViews.Find(collectionViewId);

            // Find all the children
            dynamic[] children = database.Blocks
                .Where(b => b.parent_id == collectionId)
                .Where(b => b.alive != false)
                .ToArray();

            // Apply query operators
            dynamic groupBy = parameters.query.group_by;
            dynamic calendarBy = parameters.query.calendar_by;
            JArray filters = parameters.query.filter;
            dynamic filterOperator = parameters.query.filter_operator;
            JArray sorts = parameters.query.sort;
            JArray aggregate = parameters.query.aggregate;

            Func<dynamic, string, dynamic> propertySelector = (o, property) => (o.properties as JObject)?[property];
            Func<dynamic, string, dynamic> getFinalObject = (o, property) =>
            {
                o = propertySelector(o, property);

                while (o is JArray a)
                    o = a.FirstOrDefault();

                if (o is JToken token && token.Type == JTokenType.Null)
                    return null;

                return o;
            };
            Func<dynamic, string, DateTime> getDateObject = (o, property) =>
            {
                o = propertySelector(o, property);

                while (o is JArray a)
                    o = a.LastOrDefault();

                string date = o.start_date;
                return DateTime.Parse(date);
            };

            dynamic result = new JObject();

            result.result = new JObject() {
                { "type", collectionView.type }
            };

            if (filters.Count > 0)
            {
                foreach (dynamic filter in filters)
                {
                    string property = filter.property;
                    string operation = filter.comparator;
                    dynamic value = filter.value ?? filter.value_type;

                    if (value == null || (value is JToken token && token.Type == JTokenType.Null))
                        continue;

                    Func<dynamic, bool> predicate = _ => true;

                    switch (operation)
                    {
                        case "enum_is":
                        case "string_is":
                            predicate = v => v?.ToString()?.ToLower() == value.ToString().ToLower();
                            break;
                        case "enum_is_not":
                        case "string_is_not":
                            predicate = v => v?.ToString()?.ToLower() != value.ToString().ToLower();
                            break;
                        case "string_contains": predicate = v => v?.ToString()?.ToLower()?.Contains(value.ToString().ToLower()) ?? false; break;
                        case "string_does_not_contain": predicate = v => !v?.ToString()?.ToLower()?.Contains(value.ToString().ToLower()) ?? false; break;
                        case "string_starts_with": predicate = v => v?.ToString()?.ToLower()?.StartsWith(value.ToString().ToLower()) ?? false; break;
                        case "string_ends_with": predicate = v => v?.ToString()?.ToLower()?.EndsWith(value.ToString().ToLower()) ?? false; break;
                        case "is_empty": predicate = v => string.IsNullOrWhiteSpace(v?.ToString()); break;
                        case "is_not_empty": predicate = v => !string.IsNullOrWhiteSpace(v?.ToString()); break;
                        default: Debugger.Break(); break;
                    }

                    children = children.Where(c => predicate(getFinalObject(c, property))).ToArray();
                }
            }
            if (sorts.Count > 0)
            {
                foreach (dynamic sort in sorts.Reverse())
                {
                    string property = sort.property;
                    bool direction = sort.direction == "ascending";

                    if (direction)
                        children = children.OrderBy(c => getFinalObject(c, property)).ToArray();
                    else
                        children = children.OrderByDescending(c => getFinalObject(c, property)).ToArray();
                }
            }

            if (groupBy != null)
            {
                dynamic aggregation = aggregate.First() as JObject;

                var grouped = children
                    .GroupBy(c => getFinalObject(c, (string)groupBy))
                    .Select(g =>
                    {
                        dynamic aggregationResult = null;

                        switch ((string)aggregation.aggregation_type)
                        {
                            case "count": aggregationResult = g.Count(); break;
                            default: Debugger.Break(); break;
                        }

                        dynamic groupObject = new JObject() {
                            { "total", g.Count() },
                            { "blockIds", new JArray(g.Select(b => (string)b.id).ToArray()) },
                            { "aggregationResult", new JObject() {
                                { "id", aggregation.aggregation_type },
                                { "value", aggregationResult }
                            } }
                        };

                        object key = g.Key;
                        if (key != null)
                            groupObject.value = key;

                        return groupObject;
                    })
                    .ToArray();

                result.result.groupResults = new JArray(grouped);
            }
            else if (aggregate.Count > 0)
            {
                var results = aggregate.Values<dynamic>()
                    .Select(a =>
                    {
                        string property = a.property;
                        dynamic aggregationResult = null;

                        switch ((string)a.aggregation_type)
                        {
                            case "count": aggregationResult = children.Count(); break;
                            case "empty": aggregationResult = children.Count(c => string.IsNullOrWhiteSpace(getFinalObject(c, property)?.ToString())); break;
                            case "not_empty": aggregationResult = children.Count(c => !string.IsNullOrWhiteSpace(getFinalObject(c, property)?.ToString())); break;
                            case "unique": aggregationResult = children.Select(c => getFinalObject(c, property)?.ToString()?.ToLower()).Distinct().Count(); break;
                            case "percent_empty": aggregationResult = children.Count(c => string.IsNullOrWhiteSpace(getFinalObject(c, property)?.ToString())) / (double)children.Count(); break;
                            case "percent_not_empty": aggregationResult = children.Count(c => !string.IsNullOrWhiteSpace(getFinalObject(c, property)?.ToString())) / (double)children.Count(); break;
                            case "percent_unique": aggregationResult = children.Select(c => getFinalObject(c, property)?.ToString()?.ToLower()).Distinct().Count() / (double)children.Count(); break;

                            default:
                                Debugger.Break();
                                break;
                        }

                        dynamic aggregationObject = new JObject() {
                            { "id", a.id },
                            { "value", aggregationResult }
                        };

                        return aggregationObject;
                    })
                    .ToArray();

                result.result.blockIds = new JArray(children.Select(c => (string)c.id).ToArray());
                result.result.aggregationResults = new JArray(results);
            }
            else if (calendarBy != null)
            {
                Func<long, DateTime> toDateTime = t => new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(t);
                Func<DateTime, long> fromDateTime = d => (long)(d - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;

                dynamic loader = parameters.loader;

                DateTime fromRange = toDateTime((long)loader.dateRangeStart);
                DateTime toRange = toDateTime((long)loader.dateRangeEnd);

                Dictionary<dynamic, DateTime> childrenDates = children
                    .ToDictionary(c => c, c =>
                    {
                        DateTime date = getDateObject(c, (string)calendarBy);
                        return date;
                    });

                children = childrenDates
                    .Where(p => p.Value >= fromRange && p.Value <= toRange)
                    .Select(p => p.Key)
                    .ToArray();

                TimeSpan unit = new TimeSpan(7, 0, 0, 0);

                JArray blocks = new JArray();
                
                for (; fromRange < toRange; fromRange += unit)
                {
                    var items = childrenDates
                        .Where(p => p.Value >= fromRange && p.Value < (fromRange + unit))
                        .ToArray();

                    JObject block = new JObject() {
                        { "start", fromDateTime(fromRange) },
                        { "end", fromDateTime(fromRange + unit) - 1 },
                        { "items", new JArray(items.Select((p, i) => new JObject() {
                            { "id", p.Key.id },
                            { "start", fromDateTime(p.Value) },
                            { "row", i },
                            { "includeTime", false }
                        })) }
                    };

                    blocks.Add(block);
                }

                result.result.weeks = blocks;
                result.result.noDateTotal = 0;
            }
            else
            {
                result.result.blockIds = new JArray(children.Select(c => (string)c.id).ToArray());
            }

            result.result.total = children.Length;

            result.recordMap = new JObject() {
                { "collection", new JObject() {
                    WrapObject(collection, "editor")
                } },
                { "block", new JObject(children.Select(c => WrapObject(c, "editor"))) },
                { "collection_view", new JObject() {
                    WrapObject(collection, "editor")
                } }
            };

            return result;
        }
        public object DeleteBlocks(dynamic user)
        {
            dynamic parameters = this.GetParameters();

            string[] blockIds = parameters.blockIds.ToObject<string[]>();
            bool permanentlyDelete = parameters.permanentlyDelete ?? false;

            foreach (string blockId in blockIds)
            {
                dynamic block = database.Blocks.Find(blockId);

                if (permanentlyDelete)
                    database.Blocks.Remove(block);
                else
                {
                    block.alive = false;
                    database.Blocks.Update(block);
                }
            }

            return new JObject();
        }
        public object FindUser(dynamic user)
        {
            dynamic parameters = GetParameters();

            string userEmail = parameters.email;
            dynamic matchingUser = database.Users.FirstOrDefault(u => u.email == userEmail);

            if (matchingUser == null)
                return new JObject();

            return new JObject()
            {
                { "value", new JObject() {
                    { "value", matchingUser }
                } }
            };
        }
        public object SearchGoogleContacts(dynamic user)
        {
            return new JObject()
            {
                { "results", new JArray() }
            };
        }
        public object EnqueueTask(dynamic user)
        {
            dynamic parameters = GetParameters();
            dynamic task = parameters.task;

            switch ((string)task.eventName)
            {
                case "copyBlock": taskService.CopyBlock(task); break;
                case "duplicateBlock": taskService.CopyBlock(task); break;
                case "deleteSpace": taskService.DeleteSpace(task); break;

                default:
                    Debugger.Break();
                    break;
            }

            return new JObject();
        }
        public object GetTaskResults(dynamic user)
        {
            dynamic parameters = GetParameters();

            string[] taskIds = (parameters.taskIds as JArray).Values<string>().ToArray();
            JArray results = new JArray();

            foreach (string taskId in taskIds)
            {
                TaskToken token = taskService.GetTaskResult(taskId);

                if (token == null || token.Task.Status != TaskStatus.RanToCompletion)
                {
                    results.Add(null);
                    continue;
                }

                results.Add(new JObject()
                {
                    { "result", token.Task.Result }
                });
            }

            return new JObject()
            {
                { "results", results }
            };
        }
        public object SearchPages(dynamic user)
        {
            dynamic parameters = GetParameters();

            string query = parameters.query;
            query = query?.ToLower() ?? "";

            dynamic[] pages = database.Blocks
                .Where(b => b.type == "page")
                .Where(p => p.properties?.title?.ToString()?.ToLower()?.Contains(query) ?? false)
                .Where(b => GetPermission(b, user) != null)
                .ToArray();

            JObject result = new JObject()
            {
                { "results", new JArray(pages.Select(p => p.id)) },
                { "recordMap", new JObject() {
                    { "block", new JObject(pages.Select(p => WrapObject(p, "editor"))) }
                } }
            };

            return result;
        }
        public object SearchCollections(dynamic user)
        {
            dynamic parameters = GetParameters();

            string query = parameters.query;
            query = query?.ToLower() ?? "";

            dynamic[] collections = database.Collections
                .Where(c => c.name?.ToString()?.ToLower()?.Contains(query) ?? false)
                .Where(b => GetPermission(b, user) != null)
                .ToArray();

            JObject result = new JObject()
            {
                { "results", new JArray(collections.Select(p => p.id)) },
                { "recordMap", new JObject() {
                    { "collection", new JObject(collections.Select(p => WrapObject(p, "editor"))) }
                } }
            };

            return result;
        }
        public object SearchPagesWithParent(dynamic user)
        {
            dynamic parameters = GetParameters();

            string query = parameters.query;
            query = query?.ToLower() ?? "";

            string parentId = parameters.parentId;

            int limit = parameters.limit == null ? int.MaxValue : (int)parameters.limit;

            dynamic[] pages = database.Blocks
                .Where(b => b.type == "page")
                .Where(p => p.parent_id == parentId)
                .Where(p => p.properties?.title?.ToString()?.ToLower()?.Contains(query) ?? false)
                .Where(b => GetPermission(b, user) != null)
                .Take(limit)
                .ToArray();

            JObject result = new JObject()
            {
                { "results", new JArray(pages.Select(p => p.id)) },
                { "recordMap", new JObject() {
                    { "block", new JObject(pages.Select(p => WrapObject(p, "editor"))) }
                } }
            };

            return result;
        }
        public object GetUserNotifications(dynamic user)
        {
            return new JObject()
            {
                { "results", new JArray() },
                { "recordMap", new JObject() }
            };
        }
        public object GetActivityLog(dynamic user)
        {
            return new JObject()
            {
                { "activityIds", new JArray() },
                { "recordMap", new JObject() }
            };
        }

        // To do

        public object SetSpaceInviteNotificationsAsReadAndVisited(dynamic user)
        {
            return new JObject();
        }
        public object SetPageNotificationsAsReadAndVisited(dynamic user)
        {
            return new JObject();
        }
        public object AddUsersToSpace(dynamic user)
        {
            return new JObject();
        }
        public object GetUploadFileUrl(dynamic user)
        {
            dynamic parameters = GetParameters();

            string name = parameters.name;
            name = fileService.GetUploadSlot(user, name);

            string downloadUrl = Url.Action(nameof(FileController.Download), "File", new { userId = user.id, name = name });
            string uploadUrl = Url.Action(nameof(FileController.Upload), "File", new { userId = user.id, name = name });

            dynamic result = new JObject()
            {
                { "url", downloadUrl },
                { "signedGetUrl", downloadUrl },
                { "signedPutUrl", uploadUrl }
            };

            return result;
        }
        public object GetNotificationLog(dynamic user)
        {
            return new JObject();
        }
        public object SetNotificationAsRead(dynamic user)
        {
            return new JObject();
        }
        public object SearchBlocks(dynamic user)
        {
            return new JObject();
        }
        public object GetJoinableSpaces(dynamic user)
        {
            return new JObject()
            {
                { "results", new JArray() }
            };
        }
        public object GetTrelloBoards(dynamic user)
        {
            return new JObject();
        }
        public object GetAsanaWorkspaces(dynamic user)
        {
            return new JObject();
        }
        public object SearchTrashPages(dynamic user)
        {
            return new JObject();
        }
        public object InitializeUserAnalytics(dynamic user)
        {
            return new JObject();
        }
    }
}
