﻿using Microsoft.AspNetCore.Mvc;

using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class HomeController : BaseController
    {
        private readonly AuthenticationService authentication;

        public HomeController(DatabaseService database, AuthenticationService authentication) : base(database)
        {
            this.authentication = authentication;
        }

        [ResponseCache(Duration = 604800)] // 1 week
        public IActionResult Index()
        {
            string clientId = ClientId;

            dynamic user = GetCurrentUser(authentication);
            if (user == null)
            {
                if (!authentication.Enabled)
                {
                    var token = authentication.AuthenticateDefaultUser(clientId);

                    Token = token.Token;
                    UserId = token.UserId;
                }
                else
                {
                    Token = null;
                    UserId = null;
                }
            }
            
            return View(nameof(Index));
        }

        [HttpGet("/logincallback")]
        public IActionResult LoginCallback()
        {
            return Index();
        }

        [HttpGet("/logoutcallback")]
        public IActionResult LogoutCallback()
        {
            return Index();
        }
    }
}
