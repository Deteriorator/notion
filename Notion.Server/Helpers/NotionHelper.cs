﻿using System.Diagnostics;
using System.Linq;

using Newtonsoft.Json.Linq;

using Notion.Server.Services;

namespace Notion.Server.Helpers
{
    public static class NotionHelper
    {
        public static int GetRoleLevel(string role)
        {
            switch (role)
            {
                case "none": return 0;
                case "reader": return 1;
                case "comment_only": return 2;
                case "read_and_write": return 3;
                case "editor": return 4;
            }

            Debugger.Break();
            return -1;
        }
        public static dynamic GetPermission(DatabaseService database, dynamic obj, dynamic user)
        {
            if (obj == null)
                return null;

            // If the object doesn't have any permissions, check parent
            if (obj.permissions == null)
            {
                string parentId = obj.parent_id;
                string parentTable = obj.parent_table;

                if (parentId != null && parentTable != null)
                {
                    dynamic parent = database[parentTable].Find(parentId);
                    return GetPermission(database, parent, user);
                }
                else
                {
                    return null;
                }
            }

            dynamic[] permissions = (obj.permissions as JArray).OfType<dynamic>().ToArray();

            dynamic bestPermission = null;
            int bestRoleLevel = 0;

            for (int i = 0; i < permissions.Length; i++)
            {
                dynamic permission = permissions[i];

                // Check if the permission applies
                switch ((string)permission.type)
                {
                    case "public_permission":
                        break;

                    case "space_permission":
                        dynamic space = obj;
                        while (space != null && space.parent_table != null)
                            space = database[(string)space.parent_table].Find((string)space.parent_id);

                        if (space == null)
                            continue;

                        permission = GetPermission(database, space, user);
                        if (permission == null)
                            continue;

                        break;

                    case "user_permission":
                        if (user == null || permission.user_id != user.id)
                            continue;
                        break;

                    case "group_permission":
                        continue;

                    default:
                        Debugger.Break();
                        break;
                }

                // Check permission level
                int roleLevel = GetRoleLevel((string)permission.role);
                if (roleLevel > bestRoleLevel)
                {
                    bestPermission = permission;
                    bestRoleLevel = roleLevel;
                }
            }

            return bestPermission;
        }
        public static dynamic StripPermissions(dynamic obj)
        {
            return obj;
        }

        public static JProperty WrapObject(DatabaseService database, dynamic obj, dynamic element)
        {
            string id = obj.id;
            dynamic permission = element is string ? new JObject() { { "role", element } } : GetPermission(database, obj, element);

            JProperty result = new JProperty(id, new JObject() {
                { "role", permission?.role ?? "editor" },
                { "value", permission == null ? null : obj }
            });

            return result;
        }
    }
}
