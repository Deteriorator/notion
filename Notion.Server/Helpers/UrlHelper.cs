﻿using System;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Notion.Server.Helpers
{
    public static class UrlHelper
    {
        private static bool useHttps;

        static UrlHelper()
        {
            string useHttpsText = Environment.GetEnvironmentVariable("NOTION_SERVER_HTTPS");
            useHttps = useHttpsText?.ToLower() == "true";
        }

        public static string GetBaseUrl(HttpRequest httpRequest)
        {
            string baseUrl = $"{(useHttps ? Uri.UriSchemeHttps : Uri.UriSchemeHttp)}://{httpRequest.Host}";
            return baseUrl;
        }
    }
}
